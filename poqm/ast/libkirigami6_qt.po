# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2023-05-03 23:39+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr ""

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr ""

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr ""

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr ""

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr ""

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr ""

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr ""

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr ""

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr ""

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr ""

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr ""

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr ""

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr ""

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr ""

#: controls/ActionToolBar.qml:193
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr ""

#: controls/Avatar.qml:182
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr ""

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr ""

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr ""

#: controls/GlobalDrawer.qml:506
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr ""

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr ""

#: controls/GlobalDrawer.qml:602
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr ""

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr ""

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr ""

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr ""

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr ""

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search…"
msgstr ""

#: controls/SearchField.qml:98
msgctxt "SearchField|"
msgid "Search"
msgstr ""

#: controls/SearchField.qml:109
msgctxt "SearchField|"
msgid "Clear search"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr ""

#. Accessibility text for a page tab. Keep the text as concise as possible and don't use a percent sign.
#: controls/swipenavigator/templates/PageTab.qml:38
#, qt-format
msgctxt "PageTab|"
msgid "Current page. Progress: %1 percent."
msgstr ""

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:41
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Progress: %2 percent."
msgstr ""

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:46
msgctxt "PageTab|"
msgid "Current page."
msgstr ""

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:49
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Demanding attention."
msgstr ""

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:52
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1."
msgstr ""

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr ""

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr ""

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr ""

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr ""

#: controls/ToolBarApplicationHeader.qml:89
msgctxt "ToolBarApplicationHeader|"
msgid "More Actions"
msgstr ""

#: controls/UrlButton.qml:51
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr ""

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr ""

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr ""

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr ""

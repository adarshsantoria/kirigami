# translation of libkirigami6_qt.pot to Esperanto
# Copyright (C) 2016 Free Software Foundation, Inc.
# This file is distributed under the same license as the kirigami package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kirigami\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2016-12-21 04:03+0100\n"
"PO-Revision-Date: 2023-05-07 18:25+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Sendi retmesaĝon al %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "Implikiĝi"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr "Doni"

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "Raporti Cimon"

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Kopirajto"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "Permesilo:"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Permesilo: %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Bibliotekoj en uzo"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Aŭtoroj"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "Montri aŭtorajn fotojn"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Kreditoj"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Tradukistoj"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "Pri %1"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "Forlasi"

#: controls/ActionToolBar.qml:193
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Pli da Agoj"

#: controls/Avatar.qml:182
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr "Forigi Etikedon"

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Agoj"

#: controls/GlobalDrawer.qml:506
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Reen"

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Fermi Flankbreton"

#: controls/GlobalDrawer.qml:602
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Malfermi Flankbreton"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr "Ŝargante…"

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Pasvorto"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Fermi menuon"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr "Malfermi menuon"

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search…"
msgstr "Serĉi"

#: controls/SearchField.qml:98
msgctxt "SearchField|"
msgid "Search"
msgstr "Serĉi"

#: controls/SearchField.qml:109
msgctxt "SearchField|"
msgid "Clear search"
msgstr "Klara serĉo"

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "Agordoj"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "Agordoj — %1"

#. Accessibility text for a page tab. Keep the text as concise as possible and don't use a percent sign.
#: controls/swipenavigator/templates/PageTab.qml:38
#, qt-format
msgctxt "PageTab|"
msgid "Current page. Progress: %1 percent."
msgstr "Nuna paĝo. Progreso: %1 procento."

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:41
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Progress: %2 percent."
msgstr "Iri al %1. Progreso: %2 procentoj."

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:46
msgctxt "PageTab|"
msgid "Current page."
msgstr "Aktuala paĝo."

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:49
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Demanding attention."
msgstr "Iri al %1. Postulante atenton."

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:52
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1."
msgstr "Iri al %1."

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Fermi tirkeston"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr "Malfermi tirkeston"

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Reiri Reen"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Navigi Antaŭen"

#: controls/ToolBarApplicationHeader.qml:89
msgctxt "ToolBarApplicationHeader|"
msgid "More Actions"
msgstr "Pli da Agoj"

#: controls/UrlButton.qml:51
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr "Kopii Ligilon al Tondujo"

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "La %1 fenestra sistemo"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (konstruita kontraŭ %3)"
